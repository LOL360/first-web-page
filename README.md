Important-links:
================
GIT Client : https://git-scm.com/download/win   
Jira SignUp : https://www.atlassian.com/try/cloud/signup?bundle=jira-software   
Gitlab Signup : https://gitlab.com/users/sign_in   

You can use Visual Studio Or Atom IDE

Visual Studio Code : https://code.visualstudio.com/download   

Atom : https://atom.io/download/windows_x64   
